# Changelog
All notable changes to this project will be documented in this file.

See [standard-version](https://github.com/conventional-changelog/standard-version) for commit
guidelines. This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
### [1.0.1](https://gitlab.com/felipe_public/labview-shared-libraries/json-pretty-printer.lvlib/compare/v1.0.0...v1.0.1) (2021-09-05)


### Tests

* **unit:** fixes end of line expected results ([22b5444](https://gitlab.com/felipe_public/labview-shared-libraries/json-pretty-printer.lvlib/commit/22b5444869ceddac7dd06d9cb541598bfb0873b2))
* **unit:** includes test for potential bug ([#2](https://gitlab.com/felipe_public/labview-shared-libraries/json-pretty-printer.lvlib/issues/2)) ([6467159](https://gitlab.com/felipe_public/labview-shared-libraries/json-pretty-printer.lvlib/commit/64671595b2ed2664850159c38f710cd20c5a53c9))

## [1.0.0](https://gitlab.com/felipe_public/labview-shared-libraries/json-pretty-printer.lvlib/compare/v0.1.1...v1.0.0) (2021-06-23)

### ⚠ BREAKING CHANGES

* this function changes the API and the direct upgrade
from previous version may break the code due to features inserted.
* this function deprecates the Compact Print VI. The
algorithm is recognized and tested. It removes any comments from JSON if
they are present.

### Features

* includes new function for minifying JSON ([b824530](https://gitlab.com/felipe_public/labview-shared-libraries/json-pretty-printer.lvlib/commit/b824530054a6e331ea311f3ac6786b47deafef16))


### Tests

* **unit:** add tests for pretty print and minify vis ([100ef4a](https://gitlab.com/felipe_public/labview-shared-libraries/json-pretty-printer.lvlib/commit/100ef4a50bc7e090e5554da2841c902b31df3372))


### Code Refactoring

* includes the space after : and cut trailing zeros ([eaad722](https://gitlab.com/felipe_public/labview-shared-libraries/json-pretty-printer.lvlib/commit/eaad722a0a9dfcaa38c1f2fe00e5439768749abb))


### Docs

* includes snippet and project avatar ([b93f91c](https://gitlab.com/felipe_public/labview-shared-libraries/json-pretty-printer.lvlib/commit/b93f91c9775ead19b3d414f01545a1c78aba7cec))
* updates README with better project description ([4d51b04](https://gitlab.com/felipe_public/labview-shared-libraries/json-pretty-printer.lvlib/commit/4d51b042c5cf0f0b2824f0bed92a6514d1b37dc7))


### CI

* fix to force release as major ([65800a8](https://gitlab.com/felipe_public/labview-shared-libraries/json-pretty-printer.lvlib/commit/65800a81309540498151e4e8e6c91b6833d65033))
* includes automatic release support ([55160f4](https://gitlab.com/felipe_public/labview-shared-libraries/json-pretty-printer.lvlib/commit/55160f4fc88a940e31576be01f7a455d2a7fc6c0))

## [0.1.1] - 2020-06-16
### Added
- Included Build Specification for VIPM.

## [0.1.0] - 2020-06-10
### Added
- Initial version.
